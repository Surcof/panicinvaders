local composer = require( "composer" )

local scene = composer.newScene()
--inizializza alcune variabili
local _W = display.actualContentWidth -- larghezza schermo
local _H = display.actualContentHeight -- altezza schermo
local scrollSpeed = 2



--fa partire il gioco
local function gotoGame()
	composer.gotoScene( "game", { time=800, effect="flip" } )
end
--vai ai punteggi
local function gotoHighScores()
	composer.gotoScene( "highscores", { time=800, effect="crossFade" } )
end

function scene:create( event )
   local sceneGroup = self.view


	 --carica 3 backround per fare uno sroll
	 local bg1 = display.newImageRect(sceneGroup,"background.png",  _W, _H)
	 bg1.x =display.contentCenterX
	 bg1.y = display.contentCenterY

	 local bg2 = display.newImageRect(sceneGroup, "background.png",  _W, _H)
	 bg2.x = display.contentCenterX
	 bg2.y = bg1.y+_H

	 local bg3 = display.newImageRect(sceneGroup, "background.png", _W, _H)
	 bg3.x = display.contentCenterX
	 bg3.y = bg2.y+_H

   --carica la cornice
   local frame = display.newImageRect(sceneGroup, "frame.png", _W, _H )
   frame.x = display.contentCenterX
   frame.y = display.contentCenterY
   --carica la scritta
   local dontpanic=display.newImageRect(sceneGroup, "dontpanic.png", _W*8/10 ,_W*8/20)
   dontpanic.x = display.contentCenterX
   dontpanic.y=display.contentCenterY-_H/4
   --carica il bottone start
   local start=display.newImageRect(sceneGroup,"start.png",  _W*7/10,  _W*7/20)
   start.x = display.contentCenterX
   start.y=_H*(2/3)
   start:addEventListener( "tap", gotoGame )

	 --carica il bottone highscorers
	 local highScoresButton = display.newText( sceneGroup, "High Scores", display.contentCenterX, _H*5/6, "spaceboy.ttf", _H/25 )
 		highScoresButton:setFillColor( 1, 0.01, 0 )
		highScoresButton:addEventListener( "tap", gotoHighScores )
--funzione per muovere i background
	 local function move(event)
	 	 bg1.y = bg1.y + scrollSpeed
	 	 bg2.y = bg2.y + scrollSpeed
	 	 bg3.y = bg3.y + scrollSpeed

		 --se un bg va fuori dalo schermo lo sposta in cima
	  if (bg1.y + bg1.contentHeight) > _H*3  then
	 	 bg1:translate( 0, -_H*3 )
	  end
	  if (bg2.y + bg2.contentHeight) > _H*3  then
	 	 bg2:translate( 0, -_H*3  )
	  end
	  if (bg3.y + bg3.contentHeight) > _H*3  then
	 	  bg3:translate( 0, -_H*3  )
	   end
	 end
	 --avvia il movimento
	 Runtime:addEventListener( "enterFrame", move )
end
-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
	elseif ( phase == "did" ) then
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
	elseif ( phase == "did" ) then
		Runtime:removeEventListener( "enterFrame", move )
	end
end


-- destroy()
function scene:destroy( event )
	local sceneGroup = self.view


end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene


local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
local _W = display.actualContentWidth -- larghezza schermo
local _H = display.actualContentHeight -- altezza schermo
local json = require( "json" )

local scoresTable = {}

local filePath = system.pathForFile( "scores.json", system.DocumentsDirectory )

local function loadScores()

    local file = io.open( filePath, "r" )

    if file then
        local contents = file:read( "*a" )
        io.close( file )
        scoresTable = json.decode( contents )
    end

    if ( scoresTable == nil or #scoresTable == 0 ) then
        scoresTable = { 100, 90, 80, 70, 60, 50, 40, 30, 20, 10 }
    end
end

local function saveScores()

    for i = #scoresTable, 11, -1 do
        table.remove( scoresTable, i )
    end

    local file = io.open( filePath, "w" )

    if file then
        file:write( json.encode( scoresTable ) )
        io.close( file )
    end
end
local function gotoMenu()
    composer.gotoScene( "menu", { time=800, effect="crossFade" } )
end


-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
	loadScores()

	-- Insert the saved score from the last game into the table, then reset it
    table.insert( scoresTable, composer.getVariable( "finalWave" ) )
    composer.setVariable( "finalWave", 0 )
	-- Sort the table entries from highest to lowest
  local function compare( a, b )
      return a > b
  end
  table.sort( scoresTable, compare )
	saveScores()

	local background = display.newImageRect( sceneGroup, "42ascii.png", _W, _H )
    background.x = display.contentCenterX
    background.y = display.contentCenterY

    local highScoresHeader = display.newText( sceneGroup, "High Scores", display.contentCenterX, _H/10, "spaceboy.ttf", _H/25 )
		for i = 1, 10 do
        if ( scoresTable[i] ) then
            local yPos = _H*2/10 + ( i * _H/20 )
						local rankNum = display.newText( sceneGroup, i .. ")", _W/8, yPos, "gbc.ttf", _H/25 )
            rankNum:setFillColor( 0.8 )
            rankNum.anchorX = 1

            local thisScore = display.newText( sceneGroup, scoresTable[i], _W*7/8, yPos, "digital7.ttf", _H/25 )
            thisScore.anchorX = 0

						local menuButton = display.newText( sceneGroup, "Menu", display.contentCenterX, _H*8/10, "spaceboy.ttf", _H/25 )
    				menuButton:addEventListener( "tap", gotoMenu )
        end
    end
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
		composer.removeScene( "highscores" )
	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene

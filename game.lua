local composer = require( "composer" )
local scene = composer.newScene()


----------lista funzioni-------------

-- avvia la fisica del gioco, siamo nello spazio quindi niente gravità

local physics = require ("physics")
physics.start( )
physics.setGravity( 0, 0 )
--inizializza il foglio con le immagini
local sheetOptions =
{
    frames = {

      {   --gli invaders sono dal 1 al 15
          -- invader1
          x=0,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader2
          x=33,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader3
          x=66,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader4
          x=99,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader5
          x=132,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader6
          x=165,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader7
          x=198,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader8
          x=231,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader9
          x=264,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader10
          x=297,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader11
          x=330,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader12
          x=363,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader13
          x=396,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader14
          x=429,
          y=0,
          width=32,
          height=32,

      },
      {
          -- invader15
          x=462,
          y=0,
          width=32,
          height=32,

      },
      {
          -- ship 16
          x=495,
          y=0,
          width=48,
          height=32,

      },
      {
          -- laser 17
          x=544,
          y=0,
          width=6,
          height=16,

      },
    },

}
local objectSheet = graphics.newImageSheet( "sprites.png", sheetOptions )

-------------iniziallizza le variabili---------------
local wave=1
local waveText
local ship
local gameover=false
local invadersTable = {}
local moveLeft   --movimento navicella
local move --primo movimento navicella
local grid ={} --inizzializza la griglia da dove spawnano inizialmente
--grppi per le immagini
local backGroup
local mainGroup
local uiGroup
local fireSound
local gameOverSound
local _W = display.contentWidth -- larghezza schermo
local _H = display.contentHeight -- altezza schermo
local scrollSpeed = 2 -- velocita di movimento dei background
------------------inizializza funzioni-----------

--funzione per aggiornare il counter delle waveText
local function updateText()
    waveText.text = "Wave: " .. wave
end
--movimento della navetta verso destra e poi sinistra
local function moveRight()
  move=transition.to(ship, {time=1500-wave*10, x=_W-_W/20, onComplete=moveLeft})
end
moveLeft = function ()
  move=transition.to( ship, {time=1500-wave*10, x=_W/20, onComplete=moveRight} )
end
--funzione che restituisce un posizione libera per lo spawn
local function position()
  local x=math.random(9)
  if grid[x]==nil then
    return x
  else
    return position()
  end
end
--crea gli invaders
local function createInvader()
  local newInvader=display.newImageRect( mainGroup, objectSheet, math.random(15), _W/14,_W/14  )
  local tempPos=position()
  table.insert (invadersTable, newInvader)
  physics.addBody( newInvader, "static", {radius=_W/28} )
  newInvader.myName="invader"
  newInvader.x=tempPos*_W/10
  grid[tempPos]=newInvader
  newInvader.y=1+_H/11
end

--funzione per sparare
local function fireLaser()
  if gameover==false then
    audio.play( fireSound )
    local newLaser = display.newImageRect( mainGroup, objectSheet, 17, _W/80, _H/20 )
    physics.addBody( newLaser, "dynamic", { isSensor=true } )
    newLaser.isBullet = true
    newLaser.myName = "laser"
    newLaser.x = ship.x
    newLaser.y = ship.y
    newLaser:toBack()
    transition.to( newLaser, { y=-40, time=500, onComplete = function() display.remove( newLaser ) end } )
  end
end
--funzione che chiude il gioco
local function endGame()
    Runtime:removeEventListener( "enterFrame", move )
    composer.setVariable( "finalWave", wave )
    composer.gotoScene( "highscores", { time=800, effect="crossFade" } )

end
--funzione per far scendere tutti i nemici di uno step, se un nemico arrica alla parte finale del gioco da il gameover
local function moveEnemies()
  for i = #invadersTable, 1, -1 do
   local thisInvader = invadersTable[i]
   thisInvader.y=thisInvader.y+_H/11
    if thisInvader.y>_H*10/11 then
      display.remove( ship )
      gameover=true
      audio.play( gameOverSound )
      timer.performWithDelay( 2000, endGame )
    end
  end
end
--specifica il numero di nemici da spawnare a seconda della wave attuale
local function nEnemies()
  if wave<80 then
    return math.random((wave/10)+1)
  else
    return math.random(8)
  end
end
--fa spawnare gli invader e sposta quelli presenti, aggiornando il contatore delle wave
local function newWave()
  moveEnemies()
  for i=1, nEnemies() do
    createInvader()
  end
  grid={}
  if gameover==false then
    wave=wave+1
    updateText()
  end
end

--specifica cosa accade dopo che il colpo è stato sparato
--se colpisce un invaders, lo rimuove
--se missa ed esce dallo schermo, fa spawnare nuovi invaders
local function onCollision( event )
  if ( event.phase == "began" and gameover==false ) then
    local obj1 = event.object1
    local obj2 = event.object2
    --caso laser-invader
      if ( ( obj1.myName == "laser" and obj2.myName == "invader" ) or
          ( obj1.myName == "invader" and obj2.myName == "laser" ) )
      then
      --rimuove laser e invader
       display.remove( obj1 )
       display.remove( obj2 )
      end
      --rimuove inader dalla tavola
    for i = #invadersTable, 1, -1 do
      if ( invadersTable[i] == obj1 or invadersTable[i] == obj2 ) then
        table.remove( invadersTable, i )
        --se non ci sono piu' invaders
        if #invadersTable==0 then
          timer.performWithDelay( 5, newWave )
        end
        break
      end
    end
    --caso il laser non colpisce nessun invader
    --fa partiere una nuova wave
      if ( ( obj1.myName == "laser" and obj2.myName == "missed" ) or
        ( obj1.myName == "missed" and obj2.myName == "laser" ) )
      then
        timer.performWithDelay( 1, newWave )
    end
  end
end

---------crea la scena----------
function scene:create( event )

  local sceneGroup = self.view
  physics.pause()
  --iniziallizza i gruppi
  backGroup = display.newGroup()
  sceneGroup:insert( backGroup )

  mainGroup = display.newGroup()
  sceneGroup:insert( mainGroup )

  uiGroup = display.newGroup()
  sceneGroup:insert( uiGroup )

  --crea i vari elementi a schermo

  --carica 3 backround per fare uno sroll
  local bg1 = display.newImageRect(backGroup,"background.png",  _W, _H)
  bg1.x =display.contentCenterX
  bg1.y = display.contentCenterY

  local bg2 = display.newImageRect(backGroup, "background.png",  _W, _H)
  bg2.x = display.contentCenterX
  bg2.y = bg1.y+_H

  local bg3 = display.newImageRect(backGroup, "background.png", _W, _H)
  bg3.x = display.contentCenterX
  bg3.y = bg2.y+_H
  --carica frame ui
  local frame = display.newImageRect( uiGroup, "frame.png", _W, _H)
  frame.x = display.contentCenterX
  frame.y = display.contentCenterY
  frame.isVisible=true
  --carica la navicella
  ship = display.newImageRect( mainGroup, objectSheet, 16, _W/5, _W/10 )
  ship.x =display.contentCenterX
  ship.y =_H*10/11
  --physics.addBody( ship, "static",{ radius=32, isSensor=true } )
  ship.myName = "ship"
  --testo della waveText
  waveText = display.newText( uiGroup, "Wave: " .. wave, display.contentCenterX, _H/30, "digital7.ttf",  _H/20 )
  --crea un sensore, che se il colpo non ha colpito niente, fa avanzare le wave
  local missedArea=display.newImageRect( mainGroup, "simplerectangle.png",_W, _H/11)
    physics.addBody( missedArea, "static" )
    missedArea.isVisible=false
    missedArea.x=display.contentCenterX
    missedArea.y=_H/50
    missedArea.myName="missed"
  --crea luogo dove tappare
  local controllArea=display.newImageRect( uiGroup, "simplerectangle.png", _W, _H/2.8 )
  controllArea.isVisible = false
  controllArea.isHitTestable = true
  controllArea.x=display.contentCenterX
  controllArea.y = _H
  --funzione per muovere i background
  local function move()
    if gameover==false then
      bg1.y = bg1.y + scrollSpeed
      bg2.y = bg2.y + scrollSpeed
      bg3.y = bg3.y + scrollSpeed

      --se un bg va fuori dalo schermo lo sposta in cima
      if (bg1.y + bg1.contentWidth) > _H*3 then
        bg1:translate( 0, -_H*3 )
      end
      if (bg2.y + bg2.contentWidth) > _H*3 then
        bg2:translate( 0, -_H*3 )
      end
      if (bg3.y + bg3.contentWidth) > _H*3 then
        bg3:translate( 0, -_H*3 )
      end
    end
  end

  --avvia le movimento
  Runtime:addEventListener( "enterFrame", move )
  --unico comando di gioco
  controllArea:addEventListener( "tap", fireLaser)

  fireSound = audio.loadSound( "audio/fire.mp3" )
  gameOverSound= audio.loadSound( "audio/gameover.mp3" )
end
----------gestione della scena che viene caricata--------
function scene:show( event )

  local sceneGroup = self.view
  local phase = event.phase

  if ( phase == "will" ) then

  elseif ( phase == "did" ) then
      physics.start()
      --avvia il collisioni
      Runtime:addEventListener( "collision", onCollision )

      createInvader()
      moveRight()

    end
  end


--------------gestione della scena una olta finita la partita----------
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then
        Runtime:removeEventListener( "enterFrame", move )
        Runtime:removeEventListener( "collision", onCollision )
        physics.pause()
        composer.removeScene( "game" )
    end
end
------------------
function scene:destroy( event )
	local sceneGroup = self.view
  audio.dispose( fireSound )
  audio.dispose( gameOverSound )
end
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene

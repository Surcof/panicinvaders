-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local composer = require( "composer" )
display.setStatusBar( display.HiddenStatusBar )
local musicTrack
 -- inizializza il seed del random
math.randomseed( os.time() )
--fai partire la musicTrack
musicTrack = audio.loadStream( "audio/soundtrack.mp3")
audio.reserveChannels( 1 )
audio.setVolume( 0.75, { channel=1 } )
audio.play( musicTrack, { channel=1, loops=-1 } )

-- vai al menu
composer.gotoScene( "menu" )


--[[appunto generale: tutte le immagini hanno fattori di ridimensionamento che apparentemente non hanno senso:
--sono stati ottenuti tenendo sempre presente lo schermo attuale del dispositivo, prendendo la larghezza o l'altezza e mettendola in proporsione
--per esempio se ho un immagine con rapporto 2:1 e voglio occuparne l'80% dello schermo allora avro' un
--display.newImageRect( "ciccio", display.actualContentWidth*8/10, display.actualContentWidth8/20), per il calcolo fare quindi reiferimento
--alla risoluzione dell'immagine da ridimensionare]]
